var User = require('../models/User.js');
var Auth = require('../models/Auth.js');

var authentication = (req, res, next) => {
  var query = { token: req.get('x-access-token') };
  Auth.findOne(query)
    .then(auth => {
      if (auth === null) {
        next('route');
      } else {
        User.findById(auth.user_id)
        .then(user => {
          req.user = user;
          next();
        })
        .catch(() => next('route'));
      }
    })
    .catch(() => next('route'));
};

module.exports = authentication;
