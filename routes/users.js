var express = require('express');
var bcrypt = require('bcrypt');
var router = express.Router();
var User = require('../models/User.js');
var Auth = require('../models/Auth.js');
var authentication = require('../routes/authentication.js');
var configs = require('../configs');
var saltRounds = 10;

/* GET ALL USERS */
router.get('/', authentication, function(req, res, next) {
  User.find()
  .then(users => res.send(users || []))
  .catch(() => next());
});

/* GET SINGLE USER BY ID */
router.get('/:id', authentication, (req, res, next) => {
  User.findById(req.params.id)
  .then(user => res.json(user))
  .catch(() => next());
});

/* SAVE USER */
router.post('/', async (req, res, next) => {
  try {
    const secret = req.body.secret;
    if (configs.secret !== secret) {
      throw new Error('Invalid secret');
    }
    const body = req.body;
    const existedUser = await User.findOne({username: body.username});
    if (existedUser) throw new Error('User is already existed!');
    const salt = await bcrypt.genSalt(saltRounds);
    const hash = await bcrypt.hash(body.password, salt);
    body.password = hash;
    const user = await User.create(body);
    res.json(user);
  } catch (e) {
    res.status(500);
    res.send({ error: e.message });
  }
});

/* UPDATE USER */
router.put('/:id', authentication, function(req, res, next) {
  User.findByIdAndUpdate(req.params.id, req.body)
  .then(user => res.json(user))
  .catch(() => next());
});

/* DELETE USER */
router.delete('/:id', authentication, function(req, res, next) {
  User.findByIdAndRemove(req.params.id, req.body)
  .then(user => res.json(user))
  .catch(() => next());
});

/* LOGIN USER */
router.post('/login', (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;
  User.findOne({ username })
  .then(user => {
    if (user === null) {
      res.status(500);
      res.send({ error: 'User not found' });
      return;
    }
    return bcrypt.compare(password, user.password)
    .then(success => {
      if (!success) {
        res.status(500);
        res.send({ error: 'Invalid password' });
        return;
      }
      // let token;
      // const authQuery;
      // do {
      //   token = Math.random().toString(36).slice(2);
      //   authQuery = {token: token};
      // } while (Auth.find(authQuery).length > 0)
      const token = Math.random().toString(36).slice(2);
      Auth.create({ user_id: user._id, token });
      res.json({ success: true, token });
    });
  })
  .catch(() => next());
});

/* LOGOUT USER */
router.delete('/logout', authentication, (req, res, next) => {
  Auth.remove({ user_id: req.user._id, token: req.get('x-access-token') })
  .then(() => res.send({ success: true }))
  .catch(() => next());
});

/* GET CURRENT USER */
router.get('/me', (req, res, next) => {
  var query = { token: req.get('x-access-token') };
  Auth.findOne(query)
  .then(auth => {
    if (auth == null)
  			res.json({isLoggedIn: false });
  		else {
  			User.findById(auth.user_id)
  			.then(user => res.json({isLoggedIn: true ,user: user}))
  			.catch(() => next());
  		}
  	})
  	.catch(() => next());
});

/* UPDATE CURRENT USER */
router.put('/me', function(req, res, next) {
  User.findByIdAndUpdate(req.params.id, req.body)
  .then(user => res.json(user))
  .catch(() => next());
  var query = {token: req.get('x-access-token')};
  Auth.findOne(query)
    .then(auth => {
      User.findByIdAndUpdate(auth.user_id, req.body)
      .then(user => res.json(user))
      .catch(() => next());
    })
    .catch(() => next());
});

module.exports = router;

