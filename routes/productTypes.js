var express = require('express');
var router = express.Router();
var ProductType = require('../models/ProductType.js');
var Product = require('../models/Product.js');
var authentication = require('../routes/authentication.js');

/* GET ALL PRODUCT TYPES */
router.get('/', (req, res, next) => {
  ProductType.find()
  .sort({ score: -1 })
  .then(productTypes => res.send(productTypes || []))
  .catch(() => next());
});

/* GET SINGLE PRODUCT TYPE BY ID */
router.get('/:id', authentication, (req, res, next) => {
  ProductType.findById(req.params.id)
  .then(productType => res.json(productType))
  .catch(() => next());
});

/* SAVE PRODUCT TYPE */
router.post('/', authentication, (req, res, next) => {
  ProductType.create(req.body)
  .then(productType => res.json(productType))
  .catch(() => next());
});

/* UPDATE PRODUCT TYPE */
router.put('/:id', authentication, (req, res, next) => {
  ProductType.findByIdAndUpdate(req.params.id, req.body)
  .then(productType => res.json(productType))
  .catch(() => next());
});

/* DELETE PRODUCT TYPE */
router.delete('/:id', authentication, (req, res, next) => {
  Promise.all([
    Product.update({ type_id: req.params.id }, { $unset: { type_id: '' } }),
    ProductType.findByIdAndRemove(req.params.id, req.body)
  ])
  .then(() => res.json({ success: true }))
  .catch(() => next());
});

module.exports = router;
