var express = require('express');
var router = express.Router();
var FeatureProduct = require('../models/FeatureProduct.js');
var authentication = require('../routes/authentication.js');

/* GET ALL FEATURE PRODUCTS */
router.get('/', (req, res, next) => {
  FeatureProduct.find().sort({ index: 1 })
  .then(featureProducts => res.send(featureProducts || []))
  .catch(() => next());
});

/* GET SINGLE FEATURE PRODUCT BY ID */
router.get('/:id', authentication, (req, res, next) => {
  FeatureProduct.findById(req.params.id)
  .then(featureProduct => res.json(featureProduct))
  .catch(() => next());
});

/* SAVE FEATURE PRODUCT */
router.post('/', authentication, (req, res, next) => {
  FeatureProduct.create(req.body)
  .then(featureProduct => res.json(featureProduct))
  .catch(() => next());
});

/* UPDATE FEATURE PRODUCT */
router.put('/:id', authentication, (req, res, next) => {
  FeatureProduct.findByIdAndUpdate(req.params.id, req.body, { new: true })
  .then(featureProduct => res.json(featureProduct))
  .catch(() => next());
});

/* DELETE FEATURE PRODUCT */
router.delete('/:id', authentication, (req, res, next) => {
  FeatureProduct.findByIdAndRemove(req.params.id, req.body)
  .then(featureProduct => res.json(featureProduct))
  .catch(() => next());
});

module.exports = router;
