var express = require('express');
var router = express.Router();
var Paper = require('../models/Paper.js');
var PaperType = require('../models/PaperType.js');
var authentication = require('../routes/authentication.js');

/* GET ALL PAPER */
router.get('/', authentication, (req, res, next) => {
  Paper.find().sort({ score: -1, created_at: -1 })
  .populate('type_id', 'name')
  .then(paper => res.send(paper || []))
  .catch(() => next());
});

router.get('/criteria', (req, res, next) => {
  let promise = Promise.resolve({ _id: null });
  if (req.query.type) {
    promise = PaperType.findOne({ name: req.query.type });
  }
  promise.then(paperType => {
    const skip = parseInt(req.query.skip, 10) || 0;
    const limit = parseInt(req.query.limit, 10) || 9;
    const query = paperType && paperType._id ? { type_id: paperType._id, is_archived: { $ne: true } } : { is_archived: { $ne: true } };
    return Promise.all([
      Paper.find(query).skip(skip).limit(limit).sort({ score: -1, created_at: -1 }),
      Paper.find(query).count()
    ]);
  })
  .then(([paper, total]) =>
    res.send({ paper, total })
  )
  .catch(() => next());
});

/* GET SINGLE PAPER BY SLUG */
router.get('/slug/:slug', (req, res, next) => {
  const query = { slug: req.params.slug, is_archived: { $ne: true } };
  Paper.findOne(query)
  .populate('type_id')
  .populate('related_paper_ids')
  .then(paper => {
    res.json(paper);
  })
  .catch(() => next());
});

/* GET SINGLE PAPER BY ID */
router.get('/:id', authentication, (req, res, next) => {
  Paper.findById(req.params.id)
  .then(paper => res.json(paper))
  .catch(() => next());
});

const generateSlug = (name, excepts) => {
  const slug = name.trim().toLowerCase().replace(/[^a-z\d ]/g, ' ').split(' ')
    .filter((value) => ([...excepts, ''].indexOf(value) < 0)).join('-');
  return slug;
};
const checkSlug = (paper) => {
  let slug;
  if (paper.slug) {
    slug = paper.slug;
  } else {
    slug = paper.name;
  }
  slug = generateSlug(slug, []);
  return Paper.findOne({ slug })
  .then(p => {
    if (p && p._id.toString() !== paper._id) {
      return slug + Math.random().toString(36).slice(2);
    }
    return slug;
  });
};

/* SAVE PAPER */
router.post('/', authentication, (req, res, next) => {
  checkSlug(req.body)
  .then(slug => {
    req.body.slug = slug;
    return Paper.create(req.body);
  })
  .then(paper => res.json(paper))
  .catch(() => next());
});

/* UPDATE PAPER */
router.put('/:id', authentication, (req, res, next) => {
  checkSlug(req.body)
  .then(slug => {
    req.body.slug = slug;
    return Paper.findByIdAndUpdate(req.params.id, req.body, { new: true });
  })
  .then(paper => res.json(paper))
  .catch(() => next());
});

router.post('/:id/undoDelete', authentication, (req, res, next) => {
  Paper.findByIdAndUpdate(req.params.id, { $set: { is_archived: false } }, { new: true })
  .then(paper => res.json(paper))
  .catch(() => next());
});

/* DELETE PAPER */
router.delete('/:id', authentication, (req, res, next) => {
  Paper.findByIdAndUpdate(req.params.id, { $set: { is_archived: true } }, { new: true })
  .then(paper => res.json(paper))
  .catch(() => next());
});

module.exports = router;
