var express = require('express');
var router = express.Router();
var Product = require('../models/Product.js');
var ProductType = require('../models/ProductType.js');
var authentication = require('../routes/authentication.js');

/* GET ALL PRODUCTS */
router.get('/', authentication, (req, res, next) => {
  Product.find().sort({ score: -1, created_at: -1 })
  .populate('type_id', 'name')
  .then(products => res.send(products || []))
  .catch(() => next());
});

/* GET PRODUCTS BY CRITERIA */
router.get('/criteria', (req, res, next) => {
  let promise = Promise.resolve({ _id: null });
  if (req.query.type) {
    promise = ProductType.findOne({ name: req.query.type });
  }
  promise.then(productType => {
    const skip = parseInt(req.query.skip, 10) || 0;
    const limit = parseInt(req.query.limit, 10) || 9;
    const query = productType && productType._id ? { type_id: productType._id, is_archived: { $ne: true } } : { is_archived: { $ne: true } };
    return Promise.all([
      Product.find(query).skip(skip).limit(limit).sort({ score: -1, created_at: -1 }),
      Product.find(query).count()
    ]);
  })
  .then(([products, total]) =>
    res.send({ products, total })
  )
  .catch(() => next());
});

/* GET SINGLE PRODUCT BY SLUG */
router.get('/slug/:slug', (req, res, next) => {
  const query = { slug: req.params.slug, is_archived: { $ne: true } };
  Product.findOne(query)
  .populate('type_id')
  .populate('related_product_ids')
  .then(product => {
    res.json(product);
  })
  .catch(() => next());
});

/* GET SINGLE PRODUCT BY ID */
router.get('/:id', authentication, (req, res, next) => {
  Product.findById(req.params.id)
  .then(product => res.json(product))
  .catch(() => next());
});

const generateSlug = (name, excepts) => {
  const slug = name.trim().toLowerCase().replace(/[^a-z\d ]/g, ' ').split(' ')
    .filter((value) => ([...excepts, ''].indexOf(value) < 0)).join('-');
  return slug;
};
const checkSlug = (product) => {
  let slug;
  if (product.slug) {
    slug = product.slug;
  } else {
    slug = product.name;
  }
  slug = generateSlug(slug, []);
  return Product.findOne({ slug })
  .then(p => {
    if (p && p._id.toString() !== product._id) {
      return slug + Math.random().toString(36).slice(2);
    }
    return slug;
  });
};

/* SAVE PRODUCT */
router.post('/', authentication, (req, res, next) => {
  checkSlug(req.body)
  .then(slug => {
    req.body.slug = slug;
    return Product.create(req.body);
  })
  .then(product => res.json(product))
  .catch(() => next());
});

/* UPDATE PRODUCT */
router.put('/:id', authentication, (req, res, next) => {
  checkSlug(req.body)
  .then(slug => {
    req.body.slug = slug;
    return Product.findByIdAndUpdate(req.params.id, req.body, { new: true });
  })
  .then(product => res.json(product))
  .catch(() => next());
});

router.post('/:id/undoDelete', authentication, (req, res, next) => {
  Product.findByIdAndUpdate(req.params.id, { $set: { is_archived: false } }, { new: true })
  .then(paper => res.json(paper))
  .catch(() => next());
});

/* DELETE PRODUCT */
router.delete('/:id', authentication, (req, res, next) => {
  Product.findByIdAndUpdate(req.params.id, { $set: { is_archived: true } }, { new: true })
  .then(product => res.json(product))
  .catch(() => next());
});

module.exports = router;
