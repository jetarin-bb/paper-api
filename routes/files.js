var express = require('express');
var router = express.Router();
var multer  = require('multer');
var configs = require('../configs');
var path = require('path')
const host = process.env.NODE_ENV === 'production' ? 'http://45.32.117.235:3001' : 'http://localhost:3001';

const storage = multer.diskStorage({
  destination: configs.filepath,
  filename(req, file, cb) {
  	const token = Math.random().toString(36).slice(2);
  	var fileExt = file.originalname.split('.').pop();
  	var basename = file.originalname.slice(0, -1*(fileExt.length+1));
  	var filename = basename + '-' + token + '.' + fileExt;
    cb(null, filename);
  },
});

const upload = multer({ storage });

router.get('/:filename', (req, res, next) => {
	var filename = req.params.filename;
	var filepath = '/.' + configs.filepath + '/' + filename;
	console.log(filepath);
	res.sendFile(path.join(__dirname,filepath));
});

router.post('/', upload.array('images'), (req, res, next) => {
  var initialPreview = req.files.map(file => `${host}/files/${file.filename}`);
  var initialPreviewConfig = req.files.map(file => {
    return {
      key: file.filename,
      url: `${host}/files/delete/${file.filename}`
    };
  });
  res.send({
    initialPreview: initialPreview,
    initialPreviewConfig: initialPreviewConfig,
    files: req.files
  });
});

router.post('/delete/:filename', (req, res, next) => {
  res.send({});
});

module.exports = router;