var express = require('express');
var router = express.Router();
var SliderProduct = require('../models/SliderProduct.js');
var authentication = require('../routes/authentication.js');

router.get('/', (req, res, next) => {
  SliderProduct.findOne()
  .populate('product_ids')
  .then(products => {
    if (!products || !products.product_ids) {
      return res.send([]);
    }
    return res.send(products.product_ids);
  })
  .catch(() => next());
});

router.get('/:id', authentication, (req, res, next) => {
  SliderProduct.findById(req.params.id)
  .then(product => res.json(product))
  .catch(() => next());
});

router.post('/', authentication, (req, res, next) => {
  SliderProduct.findOne()
  .then(product => {
    if (!product) {
      return SliderProduct.create(req.body);
    }
    product.product_ids = req.body.product_ids;
    return product.save();
  })
  .then(product => res.json(product))
  .catch(() => next());
});

router.put('/', authentication, (req, res, next) => {
  SliderProduct.findOne()
  .then(product => {
    if (!product) {
      return SliderProduct.create(req.body);
    }
    product.product_ids = req.body.product_ids;
    return product.save();
  })
  .then(product => res.json(product))
  .catch(() => next());
});

module.exports = router;
