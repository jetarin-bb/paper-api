var express = require('express');
var router = express.Router();
var FeaturePaper = require('../models/FeaturePaper.js');
var authentication = require('../routes/authentication.js');

/* GET ALL FEATURE PAPERS */
router.get('/', (req, res, next) => {
  FeaturePaper.findOne()
  .populate('paper_ids')
  .then(featurePapers => {
    if (!featurePapers || !featurePapers.paper_ids) {
      return res.send([]);
    }
    return res.send(featurePapers.paper_ids);
  })
  .catch(() => next());
});

 /* GET ALL PAPERS IN FEATURE PAPER */
// router.get('/papers', (req, res) => {
//   FeaturePaper.find()
//   .then(featurePapers => res.send(featurePapers[0].paper_ids || [])
//   );
// });

/* GET SINGLE FEATURE PAPER BY ID */
router.get('/:id', authentication, (req, res, next) => {
  FeaturePaper.findById(req.params.id)
  .then(featurePaper => res.json(featurePaper))
  .catch(() => next());
});

/* SAVE FEATURE PAPER */
router.post('/', authentication, (req, res, next) => {
  FeaturePaper.findOne()
  .then(featurePaper => {
    if (!featurePaper) {
      return FeaturePaper.create(req.body);
    }
    featurePaper.paper_ids = req.body.paper_ids;
    return featurePaper.save();
  })
  .then(featurePaper => res.json(featurePaper))
  .catch(() => next());
});

/* UPDATE FEATURE PAPER */
router.put('/', authentication, (req, res, next) => {
  FeaturePaper.findOne()
  .then(featurePaper => {
    if (!featurePaper) {
      return FeaturePaper.create(req.body);
    }
    featurePaper.paper_ids = req.body.paper_ids;
    return featurePaper.save();
  })
  .then(featurePaper => res.json(featurePaper))
  .catch(() => next());
});

/* DELETE FEATURE PAPER */
// router.delete('/:id', authentication, function(req, res, next) {
//   FeaturePaper.findByIdAndRemove(req.params.id, req.body)
//   .then(featurePaper => res.json(featurePaper))
//   .catch(() => next());
// });

module.exports = router;
