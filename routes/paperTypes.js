var express = require('express');
var router = express.Router();
var PaperType = require('../models/PaperType.js');
var Paper = require('../models/Paper.js');
var authentication = require('../routes/authentication.js');

/* GET ALL PAPER TYPES */
router.get('/', (req, res, next) => {
  PaperType.find()
  .sort({ score: -1 })
  .then(paperTypes => res.send(paperTypes || []))
  .catch(() => next());
});

/* GET SINGLE PAPER TYPE BY ID */
router.get('/:id', authentication, (req, res, next) => {
  PaperType.findById(req.params.id)
  .then(paperType => res.json(paperType))
  .catch(() => next());
});

/* SAVE PAPER TYPE */
router.post('/', authentication, (req, res, next) => {
  PaperType.create(req.body)
  .then(paperType => res.json(paperType))
  .catch(() => next());
});

/* UPDATE PAPER TYPE */
router.put('/:id', authentication, (req, res, next) => {
  PaperType.findByIdAndUpdate(req.params.id, req.body)
  .then(paperType => res.json(paperType))
  .catch(() => next());
});

/* DELETE PAPER TYPE */
router.delete('/:id', authentication, (req, res, next) => {
  Promise.all([
    Paper.update({ type_id: req.params.id }, { $unset: { type_id: '' } }),
    PaperType.findByIdAndRemove(req.params.id, req.body)
  ])
  .then(() => res.json({ success: true }))
  .catch(() => next());
});

module.exports = router;
