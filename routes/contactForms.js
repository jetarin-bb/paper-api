var express = require('express');
var router = express.Router();
var ContactForm = require('../models/ContactForm.js');
var authentication = require('../routes/authentication.js');
var configs = require('../configs.js');
var nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: configs.senderEmail,
    pass: configs.senderPassword,
  }
});

/* GET ALL CONTACT FORMS */
router.get('/', authentication, (req, res, next) => {
  ContactForm.find().sort({"created_at": -1})
  .then(contactForms => res.send(contactForms || []))
  .catch(() => next());
});

/* GET SINGLE CONTACT FORM BY ID */
router.get('/:id', authentication, (req, res, next) => {
  ContactForm.findById(req.params.id)
  .then(contactForm => res.json(contactForm))
  .catch(() => next());
});

/* SAVE CONTACT FORM */
router.post('/', (req, res, next) => {
  const mailOptions = {
    from: configs.senderEmail,
    to: configs.contactEmail,
    subject: `[ktpaper.co.th] - ลูกค้า ${req.body.name} ติดต่อมาจากเวปไซต์ครับ`,
    text: `ชื่อ: ${req.body.name}\nอีเมล: ${req.body.email}\nหัวข้อ: ${req.body.title}\nข้อความ: ${req.body.message}\n\nออเดอร์: ${req.body.order || '-'}`
  };
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.log(error);
    } else {
      console.log(`Email sent: ${info.response}`);
    }
  });
  ContactForm.create(req.body)
  .then(contactForm => res.json(contactForm))
  .catch(() => next());
});

/* UPDATE CONTACT FORM */
router.put('/:id', authentication, (req, res, next) => {
  ContactForm.findByIdAndUpdate(req.params.id, req.body)
  .then(contactForm => res.json(contactForm))
  .catch(() => next());
});

/* DELETE CONTACT FORM */
router.delete('/:id', authentication, (req, res, next) => {
  ContactForm.findByIdAndRemove(req.params.id, req.body)
  .then(contactForm => res.json(contactForm))
  .catch(() => next());
});

module.exports = router;
