var mongoose = require('mongoose');

const { ObjectId } = mongoose.Schema.Types;

var FeaturePaperSchema = new mongoose.Schema({
  paper_ids: [{ type: ObjectId, ref: 'Paper' }],
});

module.exports = mongoose.model('FeaturePaper', FeaturePaperSchema);
