var mongoose = require('mongoose');

const { ObjectId } = mongoose.Schema.Types;

var PaperSchema = new mongoose.Schema({
  name: String,
  type_id: { type: ObjectId, ref: 'PaperType' },
  description: String,
  slug: String,
  related_paper_ids: [{ type: ObjectId, ref: 'Paper' }],
  images: [String],
  cover_image: String,
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
  available_colors: [{
    name: String,
    color: String,
  }],
  score: Number,
  is_archived: Boolean,
});

module.exports = mongoose.model('Paper', PaperSchema);
