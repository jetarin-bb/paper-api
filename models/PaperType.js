var mongoose = require('mongoose');

var PaperTypeSchema = new mongoose.Schema({
  name: String,
  score: Number,
});

module.exports = mongoose.model('PaperType', PaperTypeSchema);
