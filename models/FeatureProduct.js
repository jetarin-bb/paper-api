var mongoose = require('mongoose');

var FeatureProductSchema = new mongoose.Schema({
  name: String,
  description: String,
  image: String,
  link: String,
  is_image_first: Boolean,
  index: Number,
});

module.exports = mongoose.model('FeatureProduct', FeatureProductSchema);
