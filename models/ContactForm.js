var mongoose = require('mongoose');

var ContactFormSchema = new mongoose.Schema({
  title: String,
  name: String,
  email: String,
  message: String,
  order: String,
  created_at: { type: Date, default: Date.now },
});

module.exports = mongoose.model('ContactForm', ContactFormSchema);
