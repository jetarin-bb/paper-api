var mongoose = require('mongoose');

const { ObjectId } = mongoose.Schema.Types;

var SliderProductSchema = new mongoose.Schema({
  product_ids: [{ type: ObjectId, ref: 'Product' }],
});

module.exports = mongoose.model('SliderProduct', SliderProductSchema);

