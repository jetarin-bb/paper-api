var mongoose = require('mongoose');

const { ObjectId } = mongoose.Schema.Types;

var AuthSchema = new mongoose.Schema({
	user_id: ObjectId,
	token: String
});

module.exports = mongoose.model('Auth', AuthSchema);