var mongoose = require('mongoose');

var ProductTypeSchema = new mongoose.Schema({
  name: String,
  score: Number,
});

module.exports = mongoose.model('ProductType', ProductTypeSchema);
