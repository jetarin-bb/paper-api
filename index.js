var express = require('express');
var cors = require('cors');
// var path = require('path');
// var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
// var restful = require('node-restful');
// var methodOverride = require('method-override');

var users = require('./routes/users');
var products = require('./routes/products');
var productTypes = require('./routes/productTypes');
var paper = require('./routes/paper');
var paperTypes = require('./routes/paperTypes');
var contactForms = require('./routes/contactForms');
var featureProducts = require('./routes/featureProducts');
var featurePaper = require('./routes/featurePaper');
var sliderProducts = require('./routes/sliderProducts');
var files = require('./routes/files');
var configs = require('./configs');
var app = express();

mongoose.Promise = global.Promise;

mongoose.connect(`mongodb://localhost/${configs.db}`)
  .then(() =>  console.log(`connect to DB ${configs.db} successful`))
  .catch((err) => console.error(err));

// view engine setup
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(cors({ credentials: true, origin: [
  'http://localhost:3002',
  'http://localhost:3000',
  'http://45.32.117.235:3000',
  'http://45.32.117.235:3002',
  'http://ktpaper.co.th',
  'http://www.ktpaper.co.th'
] }));
// app.use(cors({ credentials: true, origin: '*' }));
// app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
// app.use(bodyParser.json({type:'application/vnd.api+json'}));
// app.use(methodOverride());
app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));

// app.use('/', index);
app.use('/users', users);
app.use('/products', products);
app.use('/productTypes', productTypes);
app.use('/paper', paper);
app.use('/paperTypes', paperTypes);
app.use('/contactForms', contactForms);
app.use('/featureProducts', featureProducts); // change to featureItems
app.use('/featurePaper', featurePaper); // deprecated
app.use('/sliderProducts', sliderProducts);
app.use('/files', files);

// var Category = app.resource = restful.model('category', mongoose.Schema({
//   cat_name: String,
// }))
// .methods(['get', 'post', 'put', 'delete']);

// Category.register(app, '/category');

// catch 404 and forward to error handler

app.use((req, res) => {
  res.status(404);
  res.send({ error: 'Not found' });
});

// error handler
// app.use(function(err, req, res) {
//   console.log('ERROR3: ', err);
//   res.status(err.status || 500);
//   res.error('error');
// });

app.listen(configs.port);

module.exports = app;
