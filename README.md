
Express REST API server
=======================

Get all items

> GET /{items}

authentication required for contractFroms and users

Get item by id

> GET /{items}/{id}

authentication required

Add item

> POST /{items}

set item value to body with JSON content-type
authentication required except contractFroms and users
structure for user 

    {
    	"secret": String,
    	"user": userObj
    }

Update item

> PUT /{items}/{id}

set item value to body with JSON content-type
authentication required

Delete item

> DELETE /{items}/{id}

authentication required

Optionals
---------

Get products by criteria

> GET /products/criteria?type={typeName}&skip={skip}&limit={limit}

Get products/paper by slug

> GET /{items}/slug/{slug}

Get paper in feature papers

> GET /featurePapers/papers

User login

> POST /users/login

set item value ({username: ,password: }) to body with JSON content-type

User logout

> DELETE /users/logout

authentication required

Get current user

> GET /users/me

Update current user

> PUT /users/me

set item value to body with JSON content-type
